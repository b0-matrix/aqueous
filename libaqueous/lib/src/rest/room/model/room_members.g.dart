// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_members.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomMembers _$RoomMembersFromJson(Map<String, dynamic> json) {
  return RoomMembers()
    ..chunk = (json['chunk'] as List)
        ?.map((e) => e == null
            ? null
            : RoomMemberEvent.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$RoomMembersToJson(RoomMembers instance) =>
    <String, dynamic>{'chunk': instance.chunk};
