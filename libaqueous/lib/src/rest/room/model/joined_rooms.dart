import 'package:json_annotation/json_annotation.dart';

part 'joined_rooms.g.dart';

@JsonSerializable()
class JoinedRooms {
  @JsonKey(name: "joined_rooms")
  List<String> joinedRooms;

  static const fromJson = _$JoinedRoomsFromJson;

  Map<String, dynamic> toJson() => _$JoinedRoomsToJson(this);
}
