// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_member.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomMember _$RoomMemberFromJson(Map<String, dynamic> json) {
  return RoomMember()
    ..membership = json['membership'] as String
    ..displayName = json['displayname'] as String
    ..avatarUrl = json['avatar_url'] as String
    ..isDirect = json['is_direct'] as bool
    ..unsignedData = json['unsigned'] == null
        ? null
        : UnsignedData.fromJson(json['unsigned'] as Map<String, dynamic>);
}

Map<String, dynamic> _$RoomMemberToJson(RoomMember instance) =>
    <String, dynamic>{
      'membership': instance.membership,
      'displayname': instance.displayName,
      'avatar_url': instance.avatarUrl,
      'is_direct': instance.isDirect,
      'unsigned': instance.unsignedData
    };
