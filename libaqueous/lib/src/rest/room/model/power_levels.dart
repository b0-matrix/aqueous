import 'package:json_annotation/json_annotation.dart';

part 'power_levels.g.dart';

@JsonSerializable()
class PowerLevels {
  @JsonKey(name: "ban")
  int ban = 50;

  @JsonKey(name: "kick")
  int kick = 50;

  @JsonKey(name: "invite")
  int invite = 50;

  @JsonKey(name: "redact")
  int redact = 50;

  @JsonKey(name: "events_default")
  int events_default = 0;

  @JsonKey(name: "events")
  Map<String, int> events = {};

  @JsonKey(name: "users_default")
  int users_default = 0;

  @JsonKey(name: "users")
  Map<String, int> users = {};

  @JsonKey(name: "state_default")
  int state_default = 0;

  @JsonKey(name: "notifications")
  Map<String, dynamic> notifications = {};

  static const fromJson = _$PowerLevelsFromJson;

  Map<String, dynamic> toJson() => _$PowerLevelsToJson(this);
}
