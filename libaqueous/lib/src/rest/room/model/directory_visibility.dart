import 'package:json_annotation/json_annotation.dart';

enum DirectoryVisibility {
  @JsonKey(name: "private")
  PRIVATE,
  @JsonKey(name: "public")
  PUBLIC
}
