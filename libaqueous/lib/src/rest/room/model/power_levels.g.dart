// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'power_levels.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PowerLevels _$PowerLevelsFromJson(Map<String, dynamic> json) {
  return PowerLevels()
    ..ban = json['ban'] as int
    ..kick = json['kick'] as int
    ..invite = json['invite'] as int
    ..redact = json['redact'] as int
    ..events_default = json['events_default'] as int
    ..events = (json['events'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(k, e as int),
    )
    ..users_default = json['users_default'] as int
    ..users = (json['users'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(k, e as int),
    )
    ..state_default = json['state_default'] as int
    ..notifications = json['notifications'] as Map<String, dynamic>;
}

Map<String, dynamic> _$PowerLevelsToJson(PowerLevels instance) =>
    <String, dynamic>{
      'ban': instance.ban,
      'kick': instance.kick,
      'invite': instance.invite,
      'redact': instance.redact,
      'events_default': instance.events_default,
      'events': instance.events,
      'users_default': instance.users_default,
      'users': instance.users,
      'state_default': instance.state_default,
      'notifications': instance.notifications
    };
