// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

class _$RoomAPI extends RoomAPI {
  _$RoomAPI([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  final definitionType = RoomAPI;

  Future<Response<RoomID>> postCreateRoom(
      DirectoryVisibility visibility,
      String aliasName,
      String name,
      String topic,
      List<String> invite,
      String version,
      List<StateEvent> initialState,
      bool isDirect) {
    final $url = '/_matrix/client/r0/createRoom';
    final Map<String, dynamic> $params = {
      'visibility': visibility,
      'room_alias_name': aliasName,
      'name': name,
      'topic': topic,
      'invite': invite,
      'room_version': version,
      'initial_state': initialState,
      'is_direct': isDirect
    };
    final $request = Request('POST', $url, client.baseUrl, parameters: $params);
    return client.send<RoomID, RoomID>($request);
  }

  Future<Response> putRoomAlias(String roomAlias, String roomID) {
    final $url = '/_matrix/client/r0/directory/room/$roomAlias';
    final Map<String, dynamic> $params = {'room_id': roomID};
    final $request = Request('PUT', $url, client.baseUrl, parameters: $params);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> getRoomAlias(String roomAlias) {
    final $url = '/_matrix/client/r0/directory/room/$roomAlias';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> deleteRoomAlias(String roomAlias) {
    final $url = '/_matrix/client/r0/directory/room/$roomAlias';
    final $request = Request('DELETE', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response<JoinedRooms>> getJoinedRooms() {
    final $url = '/_matrix/client/r0/joined_rooms';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<JoinedRooms, JoinedRooms>($request);
  }

  Future<Response> postInvite(String roomID, String userID) {
    final $url = '/_matrix/client/r0/rooms/$roomID/invite';
    final Map<String, dynamic> $params = {'user_id': userID};
    final $request = Request('POST', $url, client.baseUrl, parameters: $params);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> postJoin(String roomID) {
    final $url = '/_matrix/client/r0/rooms/$roomID/join';
    final $request = Request('POST', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response<CreatedEvent>> send(
      String roomID, String eventType, String txID, dynamic content) {
    final $url = '/_matrix/client/r0/rooms/$roomID/send/$eventType/$txID';
    final $body = content;
    final $request = Request('PUT', $url, client.baseUrl, body: $body);
    return client.send<CreatedEvent, CreatedEvent>($request);
  }

  Future<Response<RoomMessages>> getMessages(String roomID, String from,
      String to, String dir, int limit, String filter) {
    final $url = '/_matrix/client/r0/rooms/$roomID/messages';
    final Map<String, dynamic> $params = {
      'from': from,
      'to': to,
      'dir': dir,
      'limit': limit,
      'filter': filter
    };
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<RoomMessages, RoomMessages>($request);
  }

  Future<Response<RoomMembers>> getMembers(String roomID) {
    final $url = '/_matrix/client/r0/rooms/$roomID/members';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<RoomMembers, RoomMembers>($request);
  }
}
