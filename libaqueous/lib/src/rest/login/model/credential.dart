import 'package:json_annotation/json_annotation.dart';

part 'credential.g.dart';

@JsonSerializable()
class Credential {
  @JsonKey(name: "user_id")
  String userID;

  @JsonKey(name: "home_server")
  String homeServer;

  @JsonKey(name: "access_token")
  String accessToken;

  @JsonKey(name: "refresh_token")
  String refreshToken;

  @JsonKey(name: "device_id")
  String deviceID;

  static const fromJson = _$CredentialFromJson;

  Map<String, dynamic> toJson() => _$CredentialToJson(this);
}
