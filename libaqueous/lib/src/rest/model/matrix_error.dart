import 'package:json_annotation/json_annotation.dart';

part 'matrix_error.g.dart';

enum MatrixErrorCode {
  M_FORBIDDEN,
  M_UNKNOWN_TOKEN,
  M_MISSING_TOKEN,
  M_BAD_JSON,
  M_NOT_JSON,
  M_NOT_FOUND,
  M_LIMIT_EXCEEDED,
  M_USER_IN_USE,
  M_ROOM_IN_USE,
  M_BAD_PAGINATION,
  M_EXCLUSIVE,
  M_UNKNOWN,
  M_TOO_LARGE,
  M_UNRECOGNIZED,
  CL_UNKNOWN_ERROR_CODE,
  CL_NONE
}

@JsonSerializable()
class MatrixError {
  @JsonKey(name: 'errcode')
  final MatrixErrorCode code;

  final String error;

  MatrixError({this.code, this.error});

  factory MatrixError.fromJson(Map<String, dynamic> json) =>
      _$MatrixErrorFromJson(json);

  Map<String, dynamic> toJson() => _$MatrixErrorToJson(this);

  String toString() => code.toString() + ": " + error;
}
