import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/event.dart';
import 'package:libaqueous/src/rest/events/model/unsigned_data.dart';

part 'room_event.g.dart';

@JsonSerializable()
class RoomEvent extends Event {
  String toString() => jsonEncode(toJson());

  static const fromJson = _$RoomEventFromJson;

  Map<String, dynamic> toJson() => _$RoomEventToJson(this);
}
