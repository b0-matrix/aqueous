// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_member_event.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomMemberEventContent _$RoomMemberEventContentFromJson(
    Map<String, dynamic> json) {
  return RoomMemberEventContent()
    ..rawContent = json['rawContent'] as Map<String, dynamic>
    ..avatarUrl = json['avatar_url'] == null
        ? null
        : Uri.parse(json['avatar_url'] as String)
    ..displayName = json['displayname'] as String
    ..membership =
        _$enumDecodeNullable(_$RoomMembershipEnumMap, json['membership']);
}

Map<String, dynamic> _$RoomMemberEventContentToJson(
        RoomMemberEventContent instance) =>
    <String, dynamic>{
      'rawContent': instance.rawContent,
      'avatar_url': instance.avatarUrl?.toString(),
      'displayname': instance.displayName,
      'membership': _$RoomMembershipEnumMap[instance.membership]
    };

T _$enumDecode<T>(Map<T, dynamic> enumValues, dynamic source) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }
  return enumValues.entries
      .singleWhere((e) => e.value == source,
          orElse: () => throw ArgumentError(
              '`$source` is not one of the supported values: '
              '${enumValues.values.join(', ')}'))
      .key;
}

T _$enumDecodeNullable<T>(Map<T, dynamic> enumValues, dynamic source) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source);
}

const _$RoomMembershipEnumMap = <RoomMembership, dynamic>{
  RoomMembership.JOIN: 'join',
  RoomMembership.LEAVE: 'leave',
  RoomMembership.INVITE: 'invite'
};

RoomMemberEvent _$RoomMemberEventFromJson(Map<String, dynamic> json) {
  return RoomMemberEvent()
    ..type = json['type'] as String
    ..eventID = json['event_id'] as String
    ..refreshToken = json['prev_content'] as Map<String, dynamic>
    ..originServerTs = json['origin_server_ts'] as int
    ..sender = json['sender'] as String
    ..stateKey = json['state_key'] as String
    ..roomID = json['room_id'] as String
    ..unsignedData = json['unsigned'] == null
        ? null
        : UnsignedData.fromJson(json['unsigned'] as Map<String, dynamic>)
    ..redacts = json['redacts'] as String
    ..content = json['content'] == null
        ? null
        : RoomMemberEventContent.fromJson(
            json['content'] as Map<String, dynamic>);
}

Map<String, dynamic> _$RoomMemberEventToJson(RoomMemberEvent instance) =>
    <String, dynamic>{
      'type': instance.type,
      'event_id': instance.eventID,
      'prev_content': instance.refreshToken,
      'origin_server_ts': instance.originServerTs,
      'sender': instance.sender,
      'state_key': instance.stateKey,
      'room_id': instance.roomID,
      'unsigned': instance.unsignedData,
      'redacts': instance.redacts,
      'content': instance.content
    };
