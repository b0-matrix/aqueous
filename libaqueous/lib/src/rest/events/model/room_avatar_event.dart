import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/event.dart';
import 'package:libaqueous/src/rest/events/model/image_info.dart';
import 'package:libaqueous/src/rest/events/model/room_state_event.dart';
import 'package:libaqueous/src/rest/events/model/unsigned_data.dart';

part 'room_avatar_event.g.dart';

@JsonSerializable()
class RoomAvatarEventContent extends EventContent {
  @JsonKey(name: "info")
  ImageInfo info;

  @JsonKey(name: "url")
  Uri url;

  static EventContent fromJson(Map<String, dynamic> json) {
    return _$RoomAvatarEventContentFromJson(json)..rawContent = json;
  }
}

@JsonSerializable()
class RoomAvatarEvent extends RoomStateEvent {
  @JsonKey(name: "content")
  covariant RoomAvatarEventContent content;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$RoomAvatarEventFromJson;

  Map<String, dynamic> toJson() => _$RoomAvatarEventToJson(this);
}
