// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_message_event.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomRichMessageEventContent _$RoomRichMessageEventContentFromJson(
    Map<String, dynamic> json) {
  return RoomRichMessageEventContent()
    ..rawContent = json['rawContent'] as Map<String, dynamic>
    ..body = json['body'] as String
    ..type = _$enumDecodeNullable(_$MessageEventTypeEnumMap, json['msgtype'])
    ..format = json['format'] as String
    ..formattedBody = json['formatted_body'] as String;
}

Map<String, dynamic> _$RoomRichMessageEventContentToJson(
        RoomRichMessageEventContent instance) =>
    <String, dynamic>{
      'rawContent': instance.rawContent,
      'body': instance.body,
      'msgtype': _$MessageEventTypeEnumMap[instance.type],
      'format': instance.format,
      'formatted_body': instance.formattedBody
    };

T _$enumDecode<T>(Map<T, dynamic> enumValues, dynamic source) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }
  return enumValues.entries
      .singleWhere((e) => e.value == source,
          orElse: () => throw ArgumentError(
              '`$source` is not one of the supported values: '
              '${enumValues.values.join(', ')}'))
      .key;
}

T _$enumDecodeNullable<T>(Map<T, dynamic> enumValues, dynamic source) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source);
}

const _$MessageEventTypeEnumMap = <MessageEventType, dynamic>{
  MessageEventType.TEXT: 'm.text',
  MessageEventType.EMOTE: 'm.emote',
  MessageEventType.NOTICE: 'm.notice',
  MessageEventType.IMAGE: 'm.image',
  MessageEventType.FILE: 'm.file',
  MessageEventType.VIDEO: 'm.video',
  MessageEventType.AUDIO: 'm.audio',
  MessageEventType.LOCATION: 'm.location'
};

RoomMessageEventContent _$RoomMessageEventContentFromJson(
    Map<String, dynamic> json) {
  return RoomMessageEventContent()
    ..rawContent = json['rawContent'] as Map<String, dynamic>
    ..body = json['body'] as String
    ..type = _$enumDecodeNullable(_$MessageEventTypeEnumMap, json['msgtype']);
}

Map<String, dynamic> _$RoomMessageEventContentToJson(
        RoomMessageEventContent instance) =>
    <String, dynamic>{
      'rawContent': instance.rawContent,
      'body': instance.body,
      'msgtype': _$MessageEventTypeEnumMap[instance.type]
    };

RoomMessageEvent _$RoomMessageEventFromJson(Map<String, dynamic> json) {
  return RoomMessageEvent()
    ..type = json['type'] as String
    ..eventID = json['event_id'] as String
    ..refreshToken = json['prev_content'] as Map<String, dynamic>
    ..originServerTs = json['origin_server_ts'] as int
    ..sender = json['sender'] as String
    ..stateKey = json['state_key'] as String
    ..roomID = json['room_id'] as String
    ..unsignedData = json['unsigned'] == null
        ? null
        : UnsignedData.fromJson(json['unsigned'] as Map<String, dynamic>)
    ..redacts = json['redacts'] as String
    ..content = json['content'] == null
        ? null
        : RoomMessageEventContent.fromJson(
            json['content'] as Map<String, dynamic>);
}

Map<String, dynamic> _$RoomMessageEventToJson(RoomMessageEvent instance) =>
    <String, dynamic>{
      'type': instance.type,
      'event_id': instance.eventID,
      'prev_content': instance.refreshToken,
      'origin_server_ts': instance.originServerTs,
      'sender': instance.sender,
      'state_key': instance.stateKey,
      'room_id': instance.roomID,
      'unsigned': instance.unsignedData,
      'redacts': instance.redacts,
      'content': instance.content
    };
