import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/room_message_event.dart';
import 'package:libaqueous/src/rest/events/model/unsigned_data.dart';

part 'room_emote_event.g.dart';

@JsonSerializable()
class RoomEmoteEvent extends RoomMessageEvent {
  @JsonKey(name: "content")
  covariant RoomMessageEventContent content;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$RoomEmoteEventFromJson;

  Map<String, dynamic> toJson() => _$RoomEmoteEventToJson(this);
}
