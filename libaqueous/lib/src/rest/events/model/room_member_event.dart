import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/event.dart';
import 'package:libaqueous/src/rest/events/model/room_state_event.dart';
import 'package:libaqueous/src/rest/events/model/unsigned_data.dart';

part 'room_member_event.g.dart';

enum RoomMembership {
  NONE,

  @JsonValue("invite")
  INVITE,
  @JsonValue("join")
  JOIN,
  @JsonValue("knock")
  KNOCK,
  @JsonValue("leave")
  LEAVE,
  @JsonValue("ban")
  BAN,
}

@JsonSerializable()
class RoomMemberEventContent extends EventContent {
  @JsonKey(name: "avatar_url")
  Uri avatarUrl;

  @JsonKey(name: "displayname")
  String displayName;

  @JsonKey(name: "membership")
  RoomMembership membership;

  static EventContent fromJson(Map<String, dynamic> json) {
    return _$RoomMemberEventContentFromJson(json)..rawContent = json;
  }
}

@JsonSerializable()
class RoomMemberEvent extends RoomStateEvent {
  @JsonKey(name: "content")
  covariant RoomMemberEventContent content;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$RoomMemberEventFromJson;

  Map<String, dynamic> toJson() => _$RoomMemberEventToJson(this);
}
