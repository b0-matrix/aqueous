import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/thumbnail_info.dart';

part 'file_info.g.dart';

@JsonSerializable()
class FileInfo {
  @JsonKey(name: "mimetype")
  String mimeType;

  @JsonKey(name: "size")
  int size;

  @JsonKey(name: "thumbnail_url")
  Uri thumbnailUrl;

  @JsonKey(name: "thumbnail_info")
  ThumbnailInfo thumbnailInfo;

  static const fromJson = _$FileInfoFromJson;

  Map<String, dynamic> toJson() => _$FileInfoToJson(this);
}
