// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'file_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FileInfo _$FileInfoFromJson(Map<String, dynamic> json) {
  return FileInfo()
    ..mimeType = json['mimetype'] as String
    ..size = json['size'] as int
    ..thumbnailUrl = json['thumbnail_url'] == null
        ? null
        : Uri.parse(json['thumbnail_url'] as String)
    ..thumbnailInfo = json['thumbnail_info'] == null
        ? null
        : ThumbnailInfo.fromJson(
            json['thumbnail_info'] as Map<String, dynamic>);
}

Map<String, dynamic> _$FileInfoToJson(FileInfo instance) => <String, dynamic>{
      'mimetype': instance.mimeType,
      'size': instance.size,
      'thumbnail_url': instance.thumbnailUrl?.toString(),
      'thumbnail_info': instance.thumbnailInfo
    };
