import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/event.dart';
import 'package:libaqueous/src/rest/events/model/room_state_event.dart';
import 'package:libaqueous/src/rest/events/model/unsigned_data.dart';

part 'room_name_event.g.dart';

@JsonSerializable()
class RoomNameEventContent extends EventContent {
  @JsonKey(name: "name")
  String name;

  static EventContent fromJson(Map<String, dynamic> json) {
    return _$RoomNameEventContentFromJson(json)..rawContent = json;
  }
}

@JsonSerializable()
class RoomNameEvent extends RoomStateEvent {
  @JsonKey(name: "content")
  covariant RoomNameEventContent content;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$RoomNameEventFromJson;

  Map<String, dynamic> toJson() => _$RoomNameEventToJson(this);
}
