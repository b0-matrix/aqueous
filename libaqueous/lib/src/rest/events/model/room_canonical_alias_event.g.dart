// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_canonical_alias_event.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomCanonicalAliasEventContent _$RoomCanonicalAliasEventContentFromJson(
    Map<String, dynamic> json) {
  return RoomCanonicalAliasEventContent()
    ..rawContent = json['rawContent'] as Map<String, dynamic>
    ..alias = json['alias'] as String;
}

Map<String, dynamic> _$RoomCanonicalAliasEventContentToJson(
        RoomCanonicalAliasEventContent instance) =>
    <String, dynamic>{
      'rawContent': instance.rawContent,
      'alias': instance.alias
    };

RoomCanonicalAliasEvent _$RoomCanonicalAliasEventFromJson(
    Map<String, dynamic> json) {
  return RoomCanonicalAliasEvent()
    ..type = json['type'] as String
    ..eventID = json['event_id'] as String
    ..refreshToken = json['prev_content'] as Map<String, dynamic>
    ..originServerTs = json['origin_server_ts'] as int
    ..sender = json['sender'] as String
    ..stateKey = json['state_key'] as String
    ..roomID = json['room_id'] as String
    ..unsignedData = json['unsigned'] == null
        ? null
        : UnsignedData.fromJson(json['unsigned'] as Map<String, dynamic>)
    ..redacts = json['redacts'] as String
    ..content = json['content'] == null
        ? null
        : RoomCanonicalAliasEventContent.fromJson(
            json['content'] as Map<String, dynamic>);
}

Map<String, dynamic> _$RoomCanonicalAliasEventToJson(
        RoomCanonicalAliasEvent instance) =>
    <String, dynamic>{
      'type': instance.type,
      'event_id': instance.eventID,
      'prev_content': instance.refreshToken,
      'origin_server_ts': instance.originServerTs,
      'sender': instance.sender,
      'state_key': instance.stateKey,
      'room_id': instance.roomID,
      'unsigned': instance.unsignedData,
      'redacts': instance.redacts,
      'content': instance.content
    };
