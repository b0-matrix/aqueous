import 'package:chopper/chopper.dart';
import 'package:libaqueous/src/rest/const.dart';
import 'package:libaqueous/src/rest/sync/model/sync_response.dart';

part "sync.chopper.dart";

@ChopperApi(baseUrl: MATRIX_CLIENT_R0)
abstract class SyncAPI extends ChopperService {
  static SyncAPI create([ChopperClient client]) => _$SyncAPI(client);

  @Get(path: "/sync")
  Future<Response<SyncResponse>> sync(
      @Query("filter") String filter,
      @Query("since") String since,
      @Query("full_state") bool fullState,
      @Query("timeout") int timeout);
}
