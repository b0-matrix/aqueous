import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'account_data_element.g.dart';

@JsonSerializable()
class AccountDataElement {
  static const String ACCOUNT_DATA_TYPE_IGNORED_USER_LIST =
      "m.ignored_user_list";
  static const String ACCOUNT_DATA_TYPE_DIRECT_MESSAGES = "m.direct";
  static const String ACCOUNT_DATA_TYPE_PREVIEW_URLS =
      "org.matrix.preview_urls";
  static const String ACCOUNT_DATA_TYPE_WIDGETS = "m.widgets";
  static const String ACCOUNT_DATA_TYPE_PUSH_RULES = "m.push_rules";

  static const String ACCOUNT_DATA_KEY_IGNORED_USERS = "ignored_users";
  static const String ACCOUNT_DATA_KEY_URL_PREVIEW_DISABLE = "disable";

  @JsonKey(name: "type")
  String type;

  @JsonKey(name: "content")
  Map<String, dynamic> content;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$AccountDataElementFromJson;

  Map<String, dynamic> toJson() => _$AccountDataElementToJson(this);
}
