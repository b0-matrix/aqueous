import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/sync/model/user_account_data.dart';

part 'user_account_data_fallback.g.dart';

@JsonSerializable()
class UserAccountDataFallback implements UserAccountData {
  @JsonKey(name: "type")
  String type;

  @JsonKey(name: "content")
  Map<String, dynamic> content;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$UserAccountDataFallbackFromJson;

  Map<String, dynamic> toJson() => _$UserAccountDataFallbackToJson(this);
}
