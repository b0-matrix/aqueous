import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/event.dart';

part 'presence_sync_response.g.dart';

@JsonSerializable()
class PresenceSyncResponse {
  @JsonKey(name: "events")
  List<Event> events;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$PresenceSyncResponseFromJson;

  Map<String, dynamic> toJson() => _$PresenceSyncResponseToJson(this);
}
