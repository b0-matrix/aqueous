// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_account_data_fallback.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserAccountDataFallback _$UserAccountDataFallbackFromJson(
    Map<String, dynamic> json) {
  return UserAccountDataFallback()
    ..type = json['type'] as String
    ..content = json['content'] as Map<String, dynamic>;
}

Map<String, dynamic> _$UserAccountDataFallbackToJson(
        UserAccountDataFallback instance) =>
    <String, dynamic>{'type': instance.type, 'content': instance.content};
