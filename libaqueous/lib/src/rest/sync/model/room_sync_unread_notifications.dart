import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/event.dart';

part 'room_sync_unread_notifications.g.dart';

@JsonSerializable()
class RoomSyncUnreadNotifications {
  @JsonKey(name: "events")
  List<Event> events;

  @JsonKey(name: "notification_count")
  int notificationCount;

  @JsonKey(name: "highlight_count")
  int highlightCount;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$RoomSyncUnreadNotificationsFromJson;

  Map<String, dynamic> toJson() => _$RoomSyncUnreadNotificationsToJson(this);
}
