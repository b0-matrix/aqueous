// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'device_list_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DeviceListResponse _$DeviceListResponseFromJson(Map<String, dynamic> json) {
  return DeviceListResponse()
    ..changed = (json['changed'] as List)?.map((e) => e as String)?.toList()
    ..left = (json['left'] as List)?.map((e) => e as String)?.toList();
}

Map<String, dynamic> _$DeviceListResponseToJson(DeviceListResponse instance) =>
    <String, dynamic>{'changed': instance.changed, 'left': instance.left};
