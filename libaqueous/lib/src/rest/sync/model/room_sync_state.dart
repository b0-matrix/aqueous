import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/event.dart';

part 'room_sync_state.g.dart';

@JsonSerializable()
class RoomSyncState {
  @JsonKey(name: "events")
  List<Event> events;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$RoomSyncStateFromJson;

  Map<String, dynamic> toJson() => _$RoomSyncStateToJson(this);
}
