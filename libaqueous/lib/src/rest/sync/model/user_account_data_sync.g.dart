// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_account_data_sync.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserAccountDataSync _$UserAccountDataSyncFromJson(Map<String, dynamic> json) {
  return UserAccountDataSync()
    ..events = (json['events'] as List)
        ?.map((e) => e == null
            ? null
            : UserAccountDataFallback.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$UserAccountDataSyncToJson(
        UserAccountDataSync instance) =>
    <String, dynamic>{'events': instance.events};
