// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_invite_state.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomInviteState _$RoomInviteStateFromJson(Map<String, dynamic> json) {
  return RoomInviteState()
    ..events = (json['events'] as List)
        ?.map(
            (e) => e == null ? null : Event.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$RoomInviteStateToJson(RoomInviteState instance) =>
    <String, dynamic>{'events': instance.events};
