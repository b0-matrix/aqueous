// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_sync_ephemeral.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomSyncEphemeral _$RoomSyncEphemeralFromJson(Map<String, dynamic> json) {
  return RoomSyncEphemeral()
    ..events = (json['events'] as List)
        ?.map(
            (e) => e == null ? null : Event.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$RoomSyncEphemeralToJson(RoomSyncEphemeral instance) =>
    <String, dynamic>{'events': instance.events};
