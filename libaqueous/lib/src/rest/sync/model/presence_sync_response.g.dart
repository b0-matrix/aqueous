// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'presence_sync_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PresenceSyncResponse _$PresenceSyncResponseFromJson(Map<String, dynamic> json) {
  return PresenceSyncResponse()
    ..events = (json['events'] as List)
        ?.map(
            (e) => e == null ? null : Event.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$PresenceSyncResponseToJson(
        PresenceSyncResponse instance) =>
    <String, dynamic>{'events': instance.events};
