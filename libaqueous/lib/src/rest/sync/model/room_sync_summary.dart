import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'room_sync_summary.g.dart';

@JsonSerializable()
class RoomSyncSummary {
  @JsonKey(name: "m.heroes")
  List<String> heroes;

  @JsonKey(name: "m.joined_member_count")
  int joinedMembersCount;

  @JsonKey(name: "m.invited_member_count")
  int invitedMembersCount;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$RoomSyncSummaryFromJson;

  Map<String, dynamic> toJson() => _$RoomSyncSummaryToJson(this);
}
