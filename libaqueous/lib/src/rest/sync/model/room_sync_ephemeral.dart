import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/event.dart';

part 'room_sync_ephemeral.g.dart';

@JsonSerializable()
class RoomSyncEphemeral {
  @JsonKey(name: "events")
  List<Event> events;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$RoomSyncEphemeralFromJson;

  Map<String, dynamic> toJson() => _$RoomSyncEphemeralToJson(this);
}
