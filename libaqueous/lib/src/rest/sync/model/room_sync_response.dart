import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/sync/model/invited_room_sync.dart';
import 'package:libaqueous/src/rest/sync/model/room_sync.dart';

part 'room_sync_response.g.dart';

@JsonSerializable()
class RoomsSyncResponse {
  @JsonKey(name: "join")
  Map<String, RoomSync> join;

  @JsonKey(name: "invite")
  Map<String, InvitedRoomSync> invite;

  @JsonKey(name: "leave")
  Map<String, RoomSync> leave;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$RoomsSyncResponseFromJson;

  Map<String, dynamic> toJson() => _$RoomsSyncResponseToJson(this);
}
