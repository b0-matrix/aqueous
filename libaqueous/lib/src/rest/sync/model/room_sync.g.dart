// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_sync.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomSync _$RoomSyncFromJson(Map<String, dynamic> json) {
  return RoomSync()
    ..state = json['state'] == null
        ? null
        : RoomSyncState.fromJson(json['state'] as Map<String, dynamic>)
    ..timeline = json['timeline'] == null
        ? null
        : RoomSyncTimeline.fromJson(json['timeline'] as Map<String, dynamic>)
    ..ephemeral = json['ephemeral'] == null
        ? null
        : RoomSyncEphemeral.fromJson(json['ephemeral'] as Map<String, dynamic>)
    ..accountData = json['account_data'] == null
        ? null
        : RoomSyncAccountData.fromJson(
            json['account_data'] as Map<String, dynamic>)
    ..unreadNotifications = json['unread_notifications'] == null
        ? null
        : RoomSyncUnreadNotifications.fromJson(
            json['unread_notifications'] as Map<String, dynamic>)
    ..summary = json['summary'] == null
        ? null
        : RoomSyncSummary.fromJson(json['summary'] as Map<String, dynamic>);
}

Map<String, dynamic> _$RoomSyncToJson(RoomSync instance) => <String, dynamic>{
      'state': instance.state,
      'timeline': instance.timeline,
      'ephemeral': instance.ephemeral,
      'account_data': instance.accountData,
      'unread_notifications': instance.unreadNotifications,
      'summary': instance.summary
    };
