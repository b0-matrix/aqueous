// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'to_device_sync_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ToDeviceSyncResponse _$ToDeviceSyncResponseFromJson(Map<String, dynamic> json) {
  return ToDeviceSyncResponse()
    ..events = (json['events'] as List)
        ?.map(
            (e) => e == null ? null : Event.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$ToDeviceSyncResponseToJson(
        ToDeviceSyncResponse instance) =>
    <String, dynamic>{'events': instance.events};
