abstract class UserAccountData {
  static const String TYPE_IGNORED_USER_LIST = "m.ignored_user_list";
  static const String TYPE_DIRECT_MESSAGES = "m.direct";
  static const String TYPE_PREVIEW_URLS = "org.matrix.preview_urls";
  static const String TYPE_WIDGETS = "m.widgets";
}
