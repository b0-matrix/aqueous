// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_sync_summary.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomSyncSummary _$RoomSyncSummaryFromJson(Map<String, dynamic> json) {
  return RoomSyncSummary()
    ..heroes = (json['m.heroes'] as List)?.map((e) => e as String)?.toList()
    ..joinedMembersCount = json['m.joined_member_count'] as int
    ..invitedMembersCount = json['m.invited_member_count'] as int;
}

Map<String, dynamic> _$RoomSyncSummaryToJson(RoomSyncSummary instance) =>
    <String, dynamic>{
      'm.heroes': instance.heroes,
      'm.joined_member_count': instance.joinedMembersCount,
      'm.invited_member_count': instance.invitedMembersCount
    };
