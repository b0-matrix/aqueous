import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/sync/model/user_account_data.dart';

part 'user_account_data_direct_message.g.dart';

@JsonSerializable()
class UserAccountDataDirectMessages implements UserAccountData {
  @JsonKey(name: "content")
  Map<String, List<String>> content;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$UserAccountDataDirectMessagesFromJson;

  Map<String, dynamic> toJson() => _$UserAccountDataDirectMessagesToJson(this);
}
