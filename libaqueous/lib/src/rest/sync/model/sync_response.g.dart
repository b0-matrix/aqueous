// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SyncResponse _$SyncResponseFromJson(Map<String, dynamic> json) {
  return SyncResponse()
    ..accountData = json['account_data'] == null
        ? null
        : UserAccountDataSync.fromJson(
            json['account_data'] as Map<String, dynamic>)
    ..nextBatch = json['next_batch'] as String
    ..presence = json['presence'] == null
        ? null
        : PresenceSyncResponse.fromJson(
            json['presence'] as Map<String, dynamic>)
    ..toDevice = json['to_device'] == null
        ? null
        : ToDeviceSyncResponse.fromJson(
            json['to_device'] as Map<String, dynamic>)
    ..rooms = json['rooms'] == null
        ? null
        : RoomsSyncResponse.fromJson(json['rooms'] as Map<String, dynamic>)
    ..deviceLists = json['device_lists'] == null
        ? null
        : DeviceListResponse.fromJson(
            json['device_lists'] as Map<String, dynamic>)
    ..deviceOneTimeKeysCount = json['device_one_time_keys_count'] == null
        ? null
        : DeviceOneTimeKeysCountSyncResponse.fromJson(
            json['device_one_time_keys_count'] as Map<String, dynamic>);
}

Map<String, dynamic> _$SyncResponseToJson(SyncResponse instance) =>
    <String, dynamic>{
      'account_data': instance.accountData,
      'next_batch': instance.nextBatch,
      'presence': instance.presence,
      'to_device': instance.toDevice,
      'rooms': instance.rooms,
      'device_lists': instance.deviceLists,
      'device_one_time_keys_count': instance.deviceOneTimeKeysCount
    };
