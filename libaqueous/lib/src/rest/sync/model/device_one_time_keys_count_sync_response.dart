import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'device_one_time_keys_count_sync_response.g.dart';

@JsonSerializable()
class DeviceOneTimeKeysCountSyncResponse {
  @JsonKey(name: "signed_curve25519")
  int signedCurve25519;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$DeviceOneTimeKeysCountSyncResponseFromJson;

  Map<String, dynamic> toJson() =>
      _$DeviceOneTimeKeysCountSyncResponseToJson(this);
}
