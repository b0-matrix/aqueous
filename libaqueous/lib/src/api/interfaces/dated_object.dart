abstract class DatedObject implements Comparable<DatedObject> {
  int date;

  @override
  int compareTo(DatedObject other) {
    return date.compareTo(other.date);
  }
}
