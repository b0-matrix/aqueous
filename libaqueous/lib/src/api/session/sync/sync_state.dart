enum SyncState {
  IDLE,
  RUNNING,
  KILLING,
  KILLED,
}
