import 'package:libaqueous/src/rest/events/model/event.dart';
import 'package:libaqueous/src/rest/events/model/room_message_event.dart';

abstract class SendService {
  Future<void> sendTextMessage(String text,
      {MessageEventType msgType: MessageEventType.TEXT,
      bool autoMarkdown = false});

  Future<void> sendFormattedTextMessage(String text, String formattedText);

  Future<void> redactEvent(Event event, String reason);
}
