abstract class StateService {
  Future<void> updateTopic(String topic);
}
