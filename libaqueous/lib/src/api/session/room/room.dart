import 'package:libaqueous/src/api/session/room/members/membership_service.dart';
import 'package:libaqueous/src/api/session/room/model/room_summary.dart';
import 'package:libaqueous/src/api/session/room/read/read_service.dart';
import 'package:libaqueous/src/api/session/room/send/send_service.dart';
import 'package:libaqueous/src/api/session/room/state/state_service.dart';
import 'package:libaqueous/src/api/session/room/timeline/timeline_service.dart';

import 'package:rxdart/rxdart.dart';

abstract class Room
    implements
        TimelineService,
        SendService,
        ReadService,
        MembershipService,
        StateService {
  String roomId;

  Observable<RoomSummary> roomSummary;
}
