abstract class ReadService {
  Future<void> markAllAsRead();

  Future<void> setReadReceipt(String eventId);

  Future<void> setReadMarker(String fullyReadEventId);
}
