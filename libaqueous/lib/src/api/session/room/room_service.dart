import 'package:libaqueous/src/api/session/room/model/room_summary.dart';
import 'package:libaqueous/src/api/session/room/room.dart';
import 'package:rxdart/rxdart.dart';

abstract class RoomService {
  Future<String> createRoom();

  Room getRoom(String roomId);

  Observable<List<RoomSummary>> get liveRoomSummaries;
}
