import 'package:libaqueous/src/api/session/room/model/event_annotations_summary.dart';
import 'package:libaqueous/src/api/session/room/send/send_state.dart';
import 'package:libaqueous/src/rest/events/model/event.dart';

class TimelineEvent {
  Event root;

  String localId;

  int displayIndex;

  String senderName;

  String senderAvatar;

  SendState sendState;

  EventAnnotationSummary annotations;
}
