enum TimelineDirection {
  /// It represents future events.
  FORWARDS,

  /// It represents past events.
  BACKWARDS
}
