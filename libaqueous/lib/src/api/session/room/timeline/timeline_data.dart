import 'package:libaqueous/src/api/session/room/timeline/timeline_event.dart';

class TimelineData {
  List<TimelineEvent> events;

  bool isLoadingForward = false;

  bool isLoadingBackward = false;
}
