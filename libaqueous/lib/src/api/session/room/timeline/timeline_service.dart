import 'package:libaqueous/src/api/session/room/timeline/timeline.dart';
import 'package:libaqueous/src/api/session/room/timeline/timeline_event.dart';

abstract class TimelineService {
  Timeline createTimeline(String eventId, {List<String> allowedTypes});

  TimelineEvent getTimeLineEvent(String eventId);
}
