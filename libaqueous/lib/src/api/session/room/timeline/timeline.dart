import 'package:libaqueous/src/api/session/room/timeline/timeline_direction.dart';

abstract class Timeline {
  void start();

  void dispose();

  bool hasMoreToLoad(TimelineDirection direction);

  void paginate(TimelineDirection direction, int count);
}
