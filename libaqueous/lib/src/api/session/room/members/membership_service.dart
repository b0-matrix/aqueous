import 'package:libaqueous/src/rest/room/model/room_member.dart';

import 'package:rxdart/rxdart.dart';

abstract class MembershipService {
  void loadRoomMembersIfNeeded();

  RoomMember get roomMember;

  Observable<List<String>> get roomMemberIdsObservable;

  Future<void> invite(String userId);

  Future<void> join();

  Future<void> leave();
}
