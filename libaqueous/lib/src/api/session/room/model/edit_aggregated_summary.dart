import 'package:libaqueous/src/rest/events/model/event.dart';

class EditAggregatedSummary {
  EventContent aggregatedContent;
  List<String> sourceEvents;
  List<String> localEchos;
  int lastEditTs = 0;
}
