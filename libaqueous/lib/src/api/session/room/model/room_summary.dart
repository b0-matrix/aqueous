import 'package:libaqueous/src/rest/events/model/event.dart';
import 'package:libaqueous/src/rest/events/model/room_member_event.dart';
import 'package:libaqueous/src/rest/events/model/room_tag.dart';

class RoomSummary {
  String roomId;
  String displayName = "";
  String topic = "";
  String avatarUrl = "";
  bool isDirect = false;
  Event lastMessage;
  List<String> otherMemberIds = [];
  int notificationCount = 0;
  int highlightCount = 0;
  List<RoomTag> tags = [];
  RoomMembership membership = RoomMembership.NONE;
}
