class ReactionAggregatedSummary {
  String key = "";
  int count = 0;
  bool addedByMe = false;
  int firstTimestamp = 0;
  List<String> sourceEvents;
  List<String> localEchoEvents;
}
