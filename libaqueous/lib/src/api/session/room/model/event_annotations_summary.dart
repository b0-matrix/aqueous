import 'package:libaqueous/src/api/session/room/model/edit_aggregated_summary.dart';
import 'package:libaqueous/src/api/session/room/model/reaction_aggregated_summary.dart';

class EventAnnotationSummary {
  String eventId;

  List<ReactionAggregatedSummary> reactionsSummary;

  EditAggregatedSummary editSummary;
}
