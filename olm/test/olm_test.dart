import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:olm/olm.dart';

void main() {
  const MethodChannel channel = MethodChannel('olm');

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await Olm.platformVersion, '42');
  });
}
