import 'dart:async';

import 'package:flutter/services.dart';

class Olm {
  static const MethodChannel _channel = const MethodChannel('olm');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}
