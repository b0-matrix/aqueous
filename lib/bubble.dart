import 'dart:convert';

import 'package:aqueous/html_widget.dart';
import 'package:aqueous/image_provider.dart';
import 'package:flutter/material.dart';
import 'package:libaqueous/libaqueous.dart';

class Bubble extends StatelessWidget {
  Bubble({this.event, this.sender, this.time, this.delivered, this.isMe})
      : assert(sender != null);

  final User sender;
  final Event event;
  final String time;
  final bool delivered, isMe;

  @override
  Widget build(BuildContext context) {
    if (event is RoomStateEvent) {
      return _buildState(context);
    }
    if (event is RoomMessageEvent) {
      if (event is RoomTextEvent ||
          event is RoomEmoteEvent ||
          event is RoomNoticeEvent) {
        return _buildTextMessage(context);
      }
      if (event is RoomImageEvent) {
        return _buildImageMessage(context);
      }
    }
    return Text(jsonEncode(event.content).toString());
  }

  Widget _buildTextMessage(BuildContext context) {
    final icon = delivered ? Icons.done : Icons.done_all;

    if (isMe) {
      return Container(
        padding: EdgeInsets.only(left: 8.0, right: 8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Container(
              padding: const EdgeInsets.all(12.0),
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      blurRadius: .5,
                      spreadRadius: 1.0,
                      color: Colors.black.withOpacity(.12))
                ],
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(5.0),
                  bottomLeft: Radius.circular(5.0),
                  bottomRight: Radius.circular(10.0),
                ),
              ),
              child: Stack(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(right: 48.0),
                    child: Html(
                      data: _eventToString(event),
                      defaultTextStyle: TextStyle(
                          color: Colors.white,
                          fontStyle: EventType.isStateEvent(event.type)
                              ? FontStyle.italic
                              : FontStyle.normal),
                      linkStyle: TextStyle(
                          color: Colors.white,
                          decoration: TextDecoration.underline),
                      onLinkTap: (_) {},
                    ),
                  ),
                  Positioned(
                    bottom: 0.0,
                    right: 0.0,
                    child: Row(
                      children: <Widget>[
                        Text(time,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 10.0,
                            )),
                        SizedBox(width: 3.0),
                        Icon(
                          icon,
                          size: 12.0,
                          color: Colors.white,
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      );
    } else {
      return Container(
        padding: EdgeInsets.only(left: 8.0, right: 8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                sender?.avatarUrl == null
                    ? CircleAvatar(
                  foregroundColor: Colors.white,
                  backgroundColor: Theme
                      .of(context)
                      .primaryColor,
                  child: Text(
                      sender?.name?.substring(0, 1)?.toUpperCase() ?? ""),
                )
                    : CircleAvatar(
                  backgroundColor: Colors.transparent,
                  backgroundImage: MatrixImage(
                      sender.client.homeServer, sender.avatarUrl,
                      width: 80, height: 80),
                ),
                Container(
                  width: 8.0,
                ),
                Flexible(
                  child: Container(
                    padding: const EdgeInsets.all(12.0),
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                            blurRadius: .5,
                            spreadRadius: 1.0,
                            color: Colors.black.withOpacity(.12))
                      ],
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(5.0),
                        bottomLeft: Radius.circular(10.0),
                        bottomRight: Radius.circular(5.0),
                      ),
                    ),
                    child: Stack(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(right: 48.0),
                          child: Html(
                            data: _eventToString(event),
                            defaultTextStyle: TextStyle(
                              color: Colors.black,
                            ),
                            onLinkTap: (_) {},
                          ),
                        ),
                        Positioned(
                          bottom: 0.0,
                          right: 0.0,
                          child: Row(
                            children: <Widget>[
                              Text(time,
                                  style: TextStyle(
                                    color: Colors.black38,
                                    fontSize: 10.0,
                                  )),
                              SizedBox(width: 3.0),
                              Icon(
                                icon,
                                size: 12.0,
                                color: Colors.black38,
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      );
    }
  }

  Widget _buildImageMessage(BuildContext context) {
    return Stack(
      children: <Widget>[
        ClipRRect(
          borderRadius: BorderRadius.circular(24.0),
          child: Image(
            image: MatrixImage(sender.client.homeServer,
                (event.content as RoomImageEventContent).url),
          ),
        ),
        Positioned(
          top: 6.0,
          left: isMe ? null : 6.0,
          right: isMe ? 6.0 : null,
          child: Container(
              padding: const EdgeInsets.all(2.0),
              child: sender?.avatarUrl == null
                  ? CircleAvatar(
                foregroundColor: Colors.white,
                backgroundColor: Theme
                    .of(context)
                    .primaryColor,
                child: Text(
                    sender?.name?.substring(0, 1)?.toUpperCase() ?? ""),
              )
                  : CircleAvatar(
                backgroundColor: Colors.transparent,
                backgroundImage: MatrixImage(
                    sender.client.homeServer, sender.avatarUrl,
                    width: 80, height: 80),
              ),
              decoration: new BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 0.6), // border color
                shape: BoxShape.circle,
              )),
        ),
      ],
    );
  }

  Widget _buildState(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        sender?.avatarUrl == null
            ? CircleAvatar(
          radius: 12.0,
          foregroundColor: Colors.white,
          backgroundColor: Theme
              .of(context)
              .primaryColor,
          child: Text(sender?.name?.substring(0, 1)?.toUpperCase() ?? ""),
        )
            : CircleAvatar(
          radius: 12.0,
          backgroundColor: Colors.transparent,
          backgroundImage: MatrixImage(
              sender.client.homeServer, sender.avatarUrl,
              width: 80, height: 80),
        ),
        Container(
          width: 4.0,
        ),
        Text(
          _eventToString(event),
          style: TextStyle(color: Theme.of(context).textTheme.caption.color),
        ),
      ],
    );
  }

  String _eventToString(Event event) {
    if (event is RoomMessageEvent) {
      if (event is RoomTextEvent ||
          event is RoomEmoteEvent ||
          event is RoomNoticeEvent) {
        if (event.content is RoomRichMessageEventContent) {
          return "<p>" +
              (event.content as RoomRichMessageEventContent).formattedBody +
              "</p>";
        }
        return event.content.body ?? "";
      } else if (event is RoomImageEvent) {
        return event.content.body ?? "";
      }
      return event.content.body ?? "";
    } else if (event is RoomMemberEvent) {
      final stateKey =
      (sender.client.users?.containsKey(event.stateKey) ?? false)
          ? sender.client.users[event.stateKey]?.name
          : null ?? sender?.name ?? "";
      switch (event.content.membership) {
        case RoomMembership.JOIN:
          return "$stateKey joined";
        case RoomMembership.LEAVE:
          return "$stateKey left";
        case RoomMembership.INVITE:
          return "$stateKey invited";
      }
      return "$stateKey unknown operation";
    }
    return "Unknown event";
  }
}
