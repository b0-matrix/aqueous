import 'package:cached_network_image/cached_network_image.dart';

class MatrixImage extends CachedNetworkImageProvider {
  MatrixImage(String homeServer, Uri imageUrl, {int width, int height})
      : super(homeServer +
            (width == null && height == null
                ? "/_matrix/media/v1/download/"
                : "/_matrix/media/v1/thumbnail/") +
            (imageUrl?.authority ?? "") +
            (imageUrl?.path ?? "") +
            (width == null && height == null
                ? ""
                : "?width=$width&height=$height"));
}
