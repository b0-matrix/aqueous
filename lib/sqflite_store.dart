import 'dart:io';

import 'package:libaqueous/libaqueous.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'dart:convert';

class SqfliteStore implements Store {
  SqfliteStore();

  String _path;

  Database _db;

  Future<void> open() async {
    _path = join(await getDatabasesPath(), "matrix.db");
    _db = await openDatabase(_path, version: 1,
        onCreate: (Database db, int version) async {
      await db
          .execute('CREATE TABLE config (key TEXT PRIMARY KEY, value TEXT)');
      await db.execute(
          'CREATE TABLE roomData (id TEXT PRIMARY KEY, name TEXT, topic TEXT, avatarUrl TEXT, unreadEventCount INTEGER, notificationCount INTEGER, highlightCount INTEGER, canonicalAlias TEXT, isDirect INTEGER, directUserID TEXT, tags TEXT, hasFullMembers INTEGER, memberIDs TEXT, userMembership TEXT)');
      await db.execute(
          'CREATE TABLE roomEvent (seq INTEGER, roomID TEXT, eventID TEXT, type TEXT, content TEXT, refreshToken TEXT, originServerTs INTEGER, sender TEXT, stateKey TEXT, unsignedData TEXT, redacts TEXT, mToken TEXT, PRIMARY KEY (seq, roomID, eventID))');
      await db
          .execute('CREATE TABLE roomToken (id TEXT PRIMARY KEY, token TEXT)');

      await db.execute(
          'CREATE TABLE userData (id TEXT PRIMARY KEY, name TEXT, avatarUrl TEXT)');
    });
  }

  Future<void> close() async {
    await _db?.close();
  }

  Future<void> commit() async {}

  Future<void> clear() async {
    await close();
    await deleteDatabase(_path);
    await open();
  }

  bool get isPermanent => false;

  Future<String> syncToken() async {
    final resp = await _db.query("config",
        columns: ["key", "value"],
        where: "key = ?",
        whereArgs: ["token"],
        limit: 1);
    if (resp.isNotEmpty) return resp.first["value"];
    return null;
  }

  Future<void> setSyncToken(String token) async {
    await _db.insert("config", {"key": "token", "value": token},
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<bool> containsRoomData(String id) async {
    final resp = await _db.query("roomData",
        columns: ["id"], where: "id = ?", whereArgs: [id], limit: 1);

    return resp.length > 0;
  }

  Future<DefaultRoomData> roomData(String id) async {
    final roomData = DefaultRoomData()
      ..id = id;

    final resp =
    await _db.query("roomData", where: "id = ?", whereArgs: [id], limit: 1);

    if (resp.isEmpty) return null;

    final firstResp = resp.first;

    roomData
      ..name = firstResp["name"]
      ..topic = firstResp["topic"]
      ..avatarUrl = (firstResp["avatarUrl"]?.toString()?.isEmpty ?? true)
          ? null
          : Uri.parse(firstResp["avatarUrl"])
      ..canonicalAlias = firstResp["canonicalAlias"]
      ..isDirect = (firstResp["isDirect"] == 1)
      ..directUserID = (firstResp["directUserID"])
      ..tags = jsonDecode(firstResp["tags"]) as Map<String, double>
      ..hasFullMembers = firstResp["hasFullMembers"] == 1
      ..memberIDs = jsonDecode(firstResp["memberIDs"]) as List<String>
      ..userMembership = firstResp["userMembership"]
      ..unreadEventCount = firstResp["unreadEventCount"]
      ..notificationCount = firstResp["notificationCount"]
      ..highlightCount = firstResp["highlightCount"];

    return roomData;
  }

  Future<Map<String, DefaultRoomData>> roomDataBulk() async {
    final resp = await _db.query("roomData");

    if (resp.isEmpty) return null;

    Map<String, DefaultRoomData> roomMap = {};

    for (final r in resp) {
      final id = r["id"];

      final roomData = DefaultRoomData()
        ..id = id
        ..name = r["name"]
        ..topic = r["topic"]
        ..avatarUrl = (r["avatarUrl"]
            ?.toString()
            ?.isEmpty ?? true)
            ? null
            : Uri.parse(r["avatarUrl"])
        ..canonicalAlias = r["canonicalAlias"]
        ..isDirect = (r["isDirect"] == 1)
        ..directUserID = (r["directUserID"])
        ..tags = (jsonDecode(r["tags"]) as Map<String, dynamic>)
            .map((String key, dynamic val) =>
            MapEntry<String, double>(key, () {
              if (val is double) return val;
              if (val is String) return double.tryParse(val);
              return 0.0;
            }()))
        ..hasFullMembers = r["hasFullMembers"] == 1
        ..memberIDs = jsonDecode(r["memberIDs"]) as List<String>
        ..userMembership = r["userMembership"]
        ..unreadEventCount = r["unreadEventCount"]
        ..notificationCount = r["notificationCount"]
        ..highlightCount = r["highlightCount"];

      roomMap[id] = roomData;
    }

    return roomMap;
  }

  Future<void> storeRoomData(String id, DefaultRoomData roomData) async {
    await _db.insert(
        "roomData",
        {
          "id": id,
          "name": roomData.name,
          "topic": roomData.topic,
          "avatarUrl":
          roomData.avatarUrl == null ? "" : roomData.avatarUrl.toString(),
          "canonicalAlias": roomData.canonicalAlias,
          "isDirect": roomData.isDirect,
          "directUserID": roomData.directUserID,
          "tags": jsonEncode(roomData.tags),
          "hasFullMembers": roomData.hasFullMembers ? 1 : 0,
          "memberIDs": jsonEncode(roomData.memberIDs),
          "userMembership": roomData.userMembership,
          "unreadEventCount": roomData.unreadEventCount,
          "notificationCount": roomData.notificationCount,
          "highlightCount": roomData.highlightCount,
        },
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<void> storeRoomDataBulk(Map<String, DefaultRoomData> roomDatas) async {
    final batch = _db.batch();

    roomDatas.forEach((id, roomData) {
      batch.insert(
          "roomData",
          {
            "id": id,
            "name": roomData.name,
            "topic": roomData.topic,
            "avatarUrl":
            roomData.avatarUrl == null ? "" : roomData.avatarUrl.toString(),
            "canonicalAlias": roomData.canonicalAlias,
            "isDirect": roomData.isDirect,
            "directUserID": roomData.directUserID,
            "tags": jsonEncode(roomData.tags),
            "hasFullMembers": roomData.hasFullMembers ? 1 : 0,
            "memberIDs": jsonEncode(roomData.memberIDs),
            "userMembership": roomData.userMembership,
            "unreadEventCount": roomData.unreadEventCount,
            "notificationCount": roomData.notificationCount,
            "highlightCount": roomData.highlightCount,
          },
          conflictAlgorithm: ConflictAlgorithm.replace);
    });

    await batch.commit(noResult: true);
  }

  Future<void> deleteRoomData(String id) async {
    await _db.delete("roomData", where: "id = ?", whereArgs: [id]);
  }

  Future<void> storeLiveRoomEvent(Event event) async {
    if (event == null || event.roomID == null || event.eventID == null) {
      print("Event is invalid: " + event.toJson().toString());
      return;
    }

    final endResp = await _db.query("roomEvent",
        columns: ["seq"],
        where: "roomID = ?",
        whereArgs: [event.roomID],
        limit: 1,
        orderBy: "seq DESC");
    var end = endResp.isEmpty ? 0 : endResp.first["end"];

    await _db.insert(
        "roomEvent",
        {
          "seq": ++end,
          "roomID": event.roomID,
          "eventID": event.eventID,
          "type": event.type,
          "content": jsonEncode(event.content),
          "refreshToken": jsonEncode(event.refreshToken),
          "originServerTs": event.originServerTs,
          "sender": event.sender,
          "stateKey": event.stateKey,
          "unsignedData": jsonEncode(event.unsignedData),
          "redacts": event.redacts,
          "mToken": event.mToken,
        },
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<void> storeRoomEvents(
      String id, TokensChunkEvents tokensChunkEvents, bool isHistory) async {
    if (id == null) return;

    var start = 0;
    final startResp = await _db.query("roomEvent",
        columns: ["seq"],
        where: "roomID = ?",
        whereArgs: [id],
        limit: 1,
        orderBy: "seq ASC");
    if (startResp.isNotEmpty) {
      start = startResp.first["seq"];
    }

    var end = 0;
    final endResp = await _db.query("roomEvent",
        columns: ["seq"],
        where: "roomID = ?",
        whereArgs: [id],
        limit: 1,
        orderBy: "seq DESC");
    if (endResp.isNotEmpty) {
      end = endResp.first["seq"];
    }

    final batch = _db.batch();

    for (final event in tokensChunkEvents.events) {
      batch.insert("roomEvent", {
        "seq": isHistory ? --start : ++end,
        "roomID": event.roomID,
        "eventID": event.eventID,
        "type": event.type,
        "content": jsonEncode(event.content),
        "refreshToken": jsonEncode(event.refreshToken),
        "originServerTs": event.originServerTs,
        "sender": event.sender,
        "stateKey": event.stateKey,
        "unsignedData": jsonEncode(event.unsignedData),
        "redacts": event.redacts,
        "mToken": event.mToken,
      });
    }

    await batch.commit(noResult: true);
  }

  Future<void> storeBackToken(String id, String token) async {
    await _db.insert("roomToken", {"id": id, "token": token},
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<TokensChunkEvents> getEarlierMessages(
      String id, String fromToken, int limit) async {
    if (id?.isEmpty ?? true) return null;

    final roomTokenResp = await _db.query("roomToken",
        columns: ["token"], where: "id = ?", whereArgs: [id], limit: 1);
    if (roomTokenResp.isNotEmpty) {
      if (fromToken == roomTokenResp.first["token"]) {
        print("Token reach top");
        return null;
      }
    }

    List<Event> subEventsList = [];

    int subEndSeq = 0;

    if (fromToken?.isEmpty ?? true) {
      final endResp = await _db.query("roomEvent",
          columns: ["seq"],
          where: "roomID = ?",
          whereArgs: [id],
          limit: 1,
          orderBy: "seq DESC");
      if (endResp.isNotEmpty) {
        subEndSeq = endResp.first["seq"];
        print("Getting latest end seq: " + subEndSeq.toString());
      }
    } else {
      final subEndSeqResp = (await _db.query("roomEvent",
          columns: ["seq"],
          where: "roomID = ? AND mToken = ?",
          whereArgs: [id, fromToken],
          limit: 1,
          orderBy: "seq ASC"));
      if (subEndSeqResp.isNotEmpty) {
        subEndSeq = subEndSeqResp.first["seq"];
      }
      subEndSeq--;
      print("Found end seq: " + subEndSeq.toString());
    }

    if (subEndSeq == null) {
      print("Unknown token");
      return null;
    }

    final eventsList = (await _db.query("roomEvent",
            where: "roomID = ? AND seq <= ?",
            whereArgs: [id, subEndSeq],
        orderBy: "seq ASC"))
        .map((val) {
      Map<String, dynamic> json = {
        "type": val["type"],
        "event_id": val["eventID"],
        "content": jsonDecode(val["content"]),
        "prev_content": jsonDecode(val["refreshToken"]),
        "origin_server_ts": val["originServerTs"],
        "sender": val["sender"],
        "state_key": val["stateKey"],
        "room_id": id,
        "unsigned": jsonDecode(val["unsignedData"]),
        "redacts": val["redacts"],
      };
      return Event.fromJson(json)
        ..mToken = val["mToken"];
    }).toList();

    if (eventsList.length == 0) return null;

    if (eventsList.length < limit) {
      subEventsList = eventsList.reversed.toList();
    } else {
      for (final event in eventsList.reversed) {
        subEventsList.add(event);
        if (subEventsList.length >= limit &&
            (event.mToken?.isNotEmpty ?? false)) break;
      }
    }

    if (subEventsList.last.mToken?.isEmpty ?? true) {
      subEventsList.last.mToken =
          (await _db.query("roomToken", where: "id = ?", whereArgs: [id]))
              .first["token"];
    }

    return TokensChunkEvents()
      ..start = subEventsList.first.mToken
      ..end = subEventsList.last.mToken
      ..events = subEventsList;
  }

  Future<bool> containsUserData(String id) async {
    final resp = await _db.query("userData",
        columns: ["id"], where: "id = ?", whereArgs: [id], limit: 1);

    return resp.length > 0;
  }

  Future<DefaultUserData> userData(String id) async {
    final userData = DefaultUserData()
      ..id = id;

    final resp =
    await _db.query("userData", where: "id = ?", whereArgs: [id], limit: 1);

    if (resp.isEmpty) return null;

    final firstResp = resp.first;

    userData
      ..id = firstResp["id"]
      ..name = firstResp["name"]
      ..avatarUrl = (firstResp["avatarUrl"]?.toString()?.isEmpty ?? true)
          ? null
          : Uri.parse(firstResp["avatarUrl"]);

    return userData;
  }

  Future<Map<String, DefaultUserData>> userDataBulk() async {
    final resp = await _db.query("userData");

    if (resp.isEmpty) return null;

    Map<String, DefaultUserData> userMap = {};

    for (final r in resp) {
      final id = r["id"];

      final userData = DefaultUserData()
        ..id = id
        ..name = r["name"]
        ..avatarUrl = (r["avatarUrl"]
            ?.toString()
            ?.isEmpty ?? true)
            ? null
            : Uri.parse(r["avatarUrl"]);

      userMap[id] = userData;
    }

    return userMap;
  }

  Future<void> storeUserData(String id, DefaultUserData userData) async {
    await _db.insert(
        "userData",
        {
          "id": id,
          "name": userData.name,
          "avatarUrl":
          userData.avatarUrl == null ? "" : userData.avatarUrl.toString(),
        },
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<void> storeUserDataBulk(Map<String, DefaultUserData> userDatas) async {
    final batch = _db.batch();

    userDatas.forEach((id, userData) {
      batch.insert(
          "userData",
          {
            "id": id,
            "name": userData.name,
            "avatarUrl":
            userData.avatarUrl == null ? "" : userData.avatarUrl.toString(),
          },
          conflictAlgorithm: ConflictAlgorithm.replace);
    });

    await batch.commit(noResult: true);
  }

  Future<void> deleteUserData(String id) async {
    await _db.delete("userData", where: "id = ?", whereArgs: [id]);
  }
}
