import 'dart:io';

import 'package:aqueous/chat.dart';
import 'package:aqueous/image_provider.dart';
import 'package:aqueous/login.dart';
import 'package:aqueous/room_list.dart';
import 'package:aqueous/settings.dart';
import 'package:aqueous/sqflite_store.dart';
import 'package:aqueous/welcome.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:libaqueous/libaqueous.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  _setTargetPlatformForDesktop();
  runApp(App());
}

void _setTargetPlatformForDesktop() {
  TargetPlatform targetPlatform;
  if (Platform.isMacOS) {
    targetPlatform = TargetPlatform.iOS;
  } else if (Platform.isLinux || Platform.isWindows) {
    targetPlatform = TargetPlatform.android;
  }
  if (targetPlatform != null) {
    debugDefaultTargetPlatformOverride = targetPlatform;
  }
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Aqueous',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(title: 'Aqueous'),
    );
  }
}

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  SharedPreferences sharedPreferences;

  Client client;

  Room currentRoom;

  _HomePageState({Key key});

  @override
  void initState() {
    super.initState();

    (() async {
      sharedPreferences = await SharedPreferences.getInstance();

      if (sharedPreferences.getString("homeserver") == null ||
          sharedPreferences.getString("userID") == null ||
          sharedPreferences.getString("accessToken") == null) {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => LoginPage(title: widget.title)),
        );
      } else {
        final homeserver = sharedPreferences.getString("homeserver");
        final userID = sharedPreferences.getString("userID");
        final accessToken = sharedPreferences.getString("accessToken");

        final store = SqfliteStore();
        await store.open();

        final c = DefaultClient(homeserver, userID, accessToken, store);

        await c.loadFromStore();

        setState(() {
          client = c;
        });

        await c.startSync();
      }
    })();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(currentRoom?.displayName ?? widget.title),
        centerTitle: true,
      ),
      drawer: Drawer(
        child: Column(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: Text(client?.myUser?.displayName ?? ""),
              accountEmail: Text(client?.myUser?.id ?? ""),
              currentAccountPicture: Container(
                padding: EdgeInsets.all(2.0),
                child: CircleAvatar(
                  backgroundColor: Colors.white,
                  backgroundImage: (client?.myUser?.avatarUrl == null ?? true)
                      ? null
                      : MatrixImage(
                      client.homeServer, client.myUser?.avatarUrl),
                  child: (client?.myUser?.avatarUrl == null ?? true)
                      ? Icon(Icons.people)
                      : null,
                ),
                decoration: BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.circle,
                ),
              ),
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
            ),
            RoomListView(
              client: client,
              onEnterRoom: (room) {
                setState(
                  () {
                    currentRoom = room;
                  },
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.settings),
              title: Text('Settings'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => SettingsPage()),
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text('Invalidate cache'),
              onTap: () async {
                client.stopSync();

                await client.store.clear();
                await client.store.close();

                exit(0);
              },
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text('Logout'),
              onTap: () async {
                client.stopSync();

                try {
                  await client.logout();
                } catch (_) {}
                await client.store.clear();
                await client.store.close();

                client = null;

                sharedPreferences.remove("homeserver");
                sharedPreferences.remove("userID");
                sharedPreferences.remove("accessToken");

                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (context) => LoginPage(title: widget.title)),
                );
              },
            ),
          ],
        ),
      ),
//      endDrawer: currentRoom == null
//          ? null
//          : Drawer(
//              child: StreamBuilder<RoomMembers>(
//                  stream: currentRoom.membersObservable,
//                  builder: (BuildContext context,
//                      AsyncSnapshot<RoomMembers> snapshot) {
//                    if (snapshot.hasError)
//                      return Text('Error: ${snapshot.error}');
//                    switch (snapshot.connectionState) {
//                      case ConnectionState.none:
//                        return CircularProgressIndicator();
//                      case ConnectionState.waiting:
//                        return CircularProgressIndicator();
//                      case ConnectionState.active:
//                        return _buildMembersView(snapshot.data);
//                      case ConnectionState.done:
//                        return _buildMembersView(snapshot.data);
//                    }
//                    return null;
//                  }),
//            ),
      body: currentRoom != null ? ChatPage(room: currentRoom) : WelcomePage(),
    );
  }

  Widget _buildMembersView(List<String> memberIDs) {
    return ListView.builder(
      itemCount: currentRoom.memberIDs.length,
      itemBuilder: (BuildContext ctx, int index) {
        final userID = memberIDs[index];
        return FutureBuilder<User>(
          future: client.getUser(userID),
          builder: (BuildContext context, AsyncSnapshot<User> snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return ListTile(
                  title: Text("No data"),
                  dense: true,
                );
              case ConnectionState.active:
              case ConnectionState.waiting:
                return ListTile(
                  title: Text("Loading"),
                  dense: true,
                );
              case ConnectionState.done:
                if (snapshot.hasError) return Text('Error: ${snapshot.error}');
                final user = snapshot.data;
                return ListTile(
                  leading: user.avatarUrl == null
                      ? CircleAvatar(
                    foregroundColor: Colors.white,
                    backgroundColor: Theme
                        .of(context)
                        .primaryColor,
                    child: Text(user.displayName
                        ?.substring(0, 1)
                        ?.toUpperCase() ??
                        ""),
                  )
                      : CircleAvatar(
                    backgroundColor: Colors.transparent,
                    backgroundImage: MatrixImage(
                        client.homeServer, user.avatarUrl,
                        width: 80, height: 80),
                  ),
                  title: Text(user.displayName ?? ""),
                  dense: true,
                  onTap: () {},
                );
            }
            return null; // unreachable
          },
        );
      },
    );
  }
}
